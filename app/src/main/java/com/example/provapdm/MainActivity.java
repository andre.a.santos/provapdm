package com.example.provapdm;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void TelaAdd(View view){
        Intent intent = new Intent(this, Add.class);
        startActivityForResult(intent,1);
    }
    public void TelaLista(View view){
        Intent intent = new Intent(this, Lista.class);
        startActivity(intent);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView saldo = findViewById(R.id.saldo);
        String s = ""+DespesaDAO.getINSTANCE().saldo();
        s = DespesaDAO.getINSTANCE().saldo()<0?"-$"+s.substring(1,s.length()):"$"+s;
        saldo.setText(s);
    }
}