package com.example.provapdm;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolderDespesa> {

    private ArrayList<Despesa> dados;
    public Adapter(ArrayList<Despesa> d){
        dados = d;
    }
    @NonNull
    @Override
    public Adapter.ViewHolderDespesa onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item, parent,false);
        ViewHolderDespesa holderDespesa = new ViewHolderDespesa(view);
        return holderDespesa;
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolderDespesa holder, int position) {
        if(dados!=null && dados.size()>0) {
            Despesa p = dados.get(position);
            holder.descricao.setText(p.getDescricao());
            holder.categoria.setText(p.getCategoria());
            holder.valor.setText("$"+p.getValor());
            holder.cor.setBackgroundColor(p.getCategoria()=="Débito"? Color.RED:Color.GREEN);
        }
    }

    @Override
    public int getItemCount() {
        return dados.size();
    }

    public class ViewHolderDespesa extends RecyclerView.ViewHolder{
        public TextView categoria, descricao, valor;
        public ImageView cor;
        public ViewHolderDespesa(@NonNull View itemView) {
            super(itemView);
            categoria = itemView.findViewById(R.id.textView2);
            descricao = itemView.findViewById(R.id.textView6);
            valor = itemView.findViewById(R.id.textView4);
            cor = itemView.findViewById(R.id.imageView);
        }
    }
}