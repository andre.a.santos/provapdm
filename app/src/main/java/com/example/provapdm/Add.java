package com.example.provapdm;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Add extends AppCompatActivity{
    List lista;
    Spinner s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        s = findViewById(R.id.spinner);
        lista = new ArrayList<String>();
        lista.add("Crédito");
        lista.add("Débito");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, lista);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(spinnerAdapter);
    }

    public void save(View view){
        EditText e = findViewById(R.id.editTextNumberDecimal);
        EditText e2 = findViewById(R.id.editTextTextMultiLine);
        if(e.getText().toString()+""!="" &&  e2.getText().toString()+""!=""){
            Despesa g = new Despesa();
            int posicao = s.getSelectedItemPosition();
            String itemSelecionado = (String) lista.get(posicao);
            g.setCategoria(itemSelecionado);

            g.setValor((Double.valueOf(e.getText().toString())));

            g.setDescricao(e2.getText().toString());
            DespesaDAO.getINSTANCE().setDespesas(g);
            finish();
        }
        else{
            Toast.makeText(this, "Preencha todos os campos!",
                    Toast.LENGTH_LONG).show();
        }

    }

}
