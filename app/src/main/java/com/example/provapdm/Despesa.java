package com.example.provapdm;

public class Despesa implements Comparable<Despesa>{
    private String categoria;
    private String descricao;
    private double valor;


    @Override
    public String toString() {
        return "Despesa{" +
                "categoria='" + categoria + '\'' +
                ", descricao='" + descricao + '\'' +
                ", valor=" + valor +
                '}';
    }

    public Despesa() {}

    public Despesa(String categoria, String descricao, double valor) {
        this.categoria = categoria;
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public int compareTo(Despesa o) {
        return this.getCategoria().toUpperCase().compareTo(o.getCategoria().toUpperCase());
    }
}
