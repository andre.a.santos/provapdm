package com.example.provapdm;
import java.util.ArrayList;

public class DespesaDAO {
    private  static DespesaDAO INSTANCE;
    private ArrayList<Despesa> despesas ;
    public DespesaDAO() {
        this.despesas = new ArrayList<>();
    }
    public static  final DespesaDAO getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE = new DespesaDAO();
        }
        return INSTANCE;
    }

    public ArrayList<Despesa> getDespesas() {
        return despesas;
    }

    public void setDespesas(Despesa despesas) {
        this.despesas.add(despesas);
    }
    public double saldo(){
        double saldo=0;
        for (Despesa g:this.despesas) {
            if(g.getCategoria()=="Crédito")
                saldo = saldo+g.getValor();
            else saldo = saldo-g.getValor();
        }

        return saldo;
    }
}