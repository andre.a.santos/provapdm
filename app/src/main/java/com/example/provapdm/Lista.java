package com.example.provapdm;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class Lista extends AppCompatActivity {
    RecyclerView list;
    Adapter adapter;
    ArrayList<Despesa> Gastao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        list = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(linearLayoutManager);
        Gastao = DespesaDAO.getINSTANCE().getDespesas();
        adapter = new Adapter(Gastao);
        list.setAdapter(adapter);
    }
    public void sorting(View v){
        Collections.sort(Gastao);
        adapter = new Adapter(Gastao);
        list.setAdapter(adapter);
    }
    public void hoje(View view){
        Date data = new Date();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String today = formatter.format(data);
        EditText e = findViewById(R.id.dia);
        e.setText(today);
    }
    public void export(View view) {
        //generate data
        StringBuilder data = new StringBuilder();
        data.append(Gastao);

        try {
            //saving the file into device
            FileOutputStream out = openFileOutput("Relatório.csv", Context.MODE_PRIVATE);
            out.write((data.toString()).getBytes());
            out.close();

            //exporting
            Context context = getApplicationContext();
            File filelocation = new File(getFilesDir(), "Relatório.csv");
            Uri path = FileProvider.getUriForFile(context, "com.example.exportcsv.fileprovider", filelocation);
            Intent fileIntent = new Intent(Intent.ACTION_SEND);
            fileIntent.setType("text/csv");
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Data");
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            fileIntent.putExtra(Intent.EXTRA_STREAM, path);
            startActivity(Intent.createChooser(fileIntent, "Send mail"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}